import mysql from 'mysql'

const DB_SECRET = process.env.DB_SECRET
const DB_NAME = process.env.DB_NAME

const connection = {
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME
}

if (process.env.DATABASE_SOCKET) {
    connection.socketPath = process.env.DATABASE_SOCKET
} else {
    connection.host = process.env.DB_HOST
}

const conn = mysql.createConnection(connection)

console.log(DB_SECRET, DB_NAME)

conn.connect(function(err) {
    if (err) {
        console.error('error connecting: ' + err.stack)
        return
    }

    console.log('Database Connected')
})

export default conn