import express from "express";
import {checkToken} from "../util.js";
import { createUser } from "../database.js";
import { v4 as uuidv4 } from "uuid";
import { createRequire } from 'module';
const require = createRequire(import.meta.url);
const bcrypt = require("bcrypt");
const saltRounds = 10;
const router = express.Router();

router.post("/", checkToken, (req, res) => {
    
    let newUser = {
        id: uuidv4(),
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
    };

    bcrypt.hash(req.body.password, saltRounds).then(function (hash) {
        newUser.password = hash;
        
        createUser(newUser)
        .then( () => {
            return res.status(201).json(newUser)
        })
    })
});

export default router;