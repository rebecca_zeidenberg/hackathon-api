## Installation

1. Run `npm install` to install dependencies.
2. Run`database/schema.sql` and `database/seeds.sql` against database.
3. Run `npm run dev` to start server. 


## Deploying the API to GCP

1. Run `docker compose build` to build the Dockerfile from `docker-compose.yml` and run containers.
2. Run `docker compose up` to start the containers in the background and keep them running.
3. In GCP, create a SQL instance and set the local time zone and local IP address.
4. Back in the terminal, run `docker build -t courseproject .` if the containers are successfully running to build the image.
5. Run `docker tag courseproject gcr.io/course-project-317822/courseproject` to tag the image appropriately with its GCP ID.
6. Run `docker push gcr.io/course-project-317822/courseproject` to push the image up to the GCP container specified by its ID. Here it is `gcr.io/course-project-317822/courseproject`.
7. To deploy the API, run `gcloud run deploy courseproject --image gcr.io/course-project-317822/courseproject --add-cloudsql-instances course-project-317822:northamerica-northeast1:course-project --update-env-vars DATABASE_SOCKET=/cloudsql/course-project-317822:northamerica-northeast1:course-project,DB_USER=root,DB_PASSWORD=password,DB_NAME=courseproject,ENV_PORT=8080 --platform=managed`. This command identifies the project by its unique Google ID, its SQL instance, and the database name, username, and password, and identifies the appropriate port to map to.
8. Create a bucket in GCP with the static files from the build folder from the front-end project and change the website configuration setting found in the kabob menu `Index (main) page suffix` to `index.html` and `Error (404 not found) page` to default to `index.html`.
9. Set static items in the bucket to public by going to the `Permissions` tab and click `+ Add`. In this page, set `New Members` to `allUsers`, find their role under `Cloud Storage` and select `Storage Object Viewer` to make all items in the bucket public.
10. Check if website is up and running at `http://1040-course-project.1040-courseproject-rz.xyz/`.
11. To enter website as an admin, use email `address@email.com` and password `test`.