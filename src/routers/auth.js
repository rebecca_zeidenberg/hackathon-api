import express from "express";
import { getUserByEmail } from "../database.js";
import { createRequire } from 'module';
const require = createRequire(import.meta.url);
const bcrypt = require("bcrypt");
let jwt = require("jsonwebtoken");

const router = express.Router();

router.post("/", (req, res) => {
    const email = req.body.username
    const password = req.body.password
    getUserByEmail(email)
    .then(user => {
        if (!user) {
            return res.status(404).json()
        };
        bcrypt.compare(password, user.password).then (function (result) {
            if (!result) {
                return res.status(400).json({message: "incorrect credentials provided"})
            }
            let token = jwt.sign(user, "string");
            return res.status(201).json({token});
        })
    })
})

export default router;