use courseproject;

insert into contactinfo (name, email, phoneNumber, message)
values
('Bob Smith', 'bobsmith@mail.com', '555-555-5555', 'gnrwrgwbwjgnwrigkhwrgbwgwbrgjwkbgk3rjrbg3w'),
('Carol Kim', 'carolekim@mail.com', '666-666-6666', 'gnorwgbworgbrwubgrwjg'),
('Pat Thompson', 'pthompson@mail.com', '444-444-4444', 'ghnwkgwgbwgbwhtwui4tyujgbubolbvnakbg'),
('Chris Hill', 'chrishill123@mail.com', '777-777-7777', 'nglkenglkwngwklrngwklrgw'),
('Bob Smith', 'bobsmith@mail.com', '555-555-5555', 'gnrwrgwbwjgnwrigkhwrgbwgwbrgjwkbgk3rjrbg3w'),
('Carol Kim', 'carolekim@mail.com', '666-666-6666', 'gnorwgbworgbrwubgrwjg'),
('Pat Thompson', 'pthompson@mail.com', '444-444-4444', 'ghnwkgwgbwgbwhtwui4tyujgbubolbvnakbg'),
('Chris Hill', 'chrishill123@mail.com', '777-777-7777', 'nglkenglkwngwklrngwklrgw');

insert into portfolio (image, description)
values
('/pictures/portimage1.jpg', 'Pastel drawing of girls riding a tiger and zebra'),
('/pictures/portimage2.png', 'Late 19th century painting of ballet dancers'),
('/pictures/portimage3.png', 'Colourful midcentury painting of flowers'),
('/pictures/portimage4.jpg', 'Late 19th century Manet painting, looks like oil, of a woman, a boy, and a man outside'),
('/pictures/portimage5.jpg', 'Likely early 20th century modernist painting of a European countryside');

insert into resume (title, image, description)
values
('About', '/pictures/landscape1.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ornare lectus sit amet est placerat in. Pharetra magna ac placerat vestibulum lectus. Vel risus commodo viverra maecenas. Ultricies mi eget mauris pharetra. Leo urna molestie at elementum eu. In cursus turpis massa tincidunt dui ut ornare lectus sit. Ipsum dolor sit amet consectetur adipiscing elit duis tristique sollicitudin. Scelerisque varius morbi enim nunc faucibus. Nisl suscipit adipiscing bibendum est. Augue mauris augue neque gravida in.'),
('Our Story', '/pictures/landscape2.png', 'Facilisis leo vel fringilla est ullamcorper eget nulla. Sapien pellentesque habitant morbi tristique senectus. Justo donec enim diam vulputate. Mauris ultrices eros in cursus turpis. Nec nam aliquam sem et tortor consequat id porta. Diam maecenas sed enim ut sem viverra aliquet eget. Massa sapien faucibus et molestie ac feugiat sed lectus. Tortor at auctor urna nunc id cursus metus aliquam. Condimentum id venenatis a condimentum vitae sapien pellentesque habitant. Quam elementum pulvinar etiam non quam lacus suspendisse. Volutpat sed cras ornare arcu dui vivamus arcu. Sed tempus urna et pharetra pharetra massa massa ultricies mi.'),
('Current Projects', '/pictures/landscape3.jpg', 'Nisi vitae suscipit tellus mauris a diam maecenas sed enim. Ut tellus elementum sagittis vitae. At elementum eu facilisis sed odio morbi. Vel quam elementum pulvinar etiam non quam lacus suspendisse faucibus. Accumsan sit amet nulla facilisi morbi tempus iaculis urna id. Gravida cum sociis natoque penatibus et magnis dis. Dui ut ornare lectus sit amet est placerat. Facilisi cras fermentum odio eu feugiat pretium.');