import express from "express";
import contactForm from "./routers/contact.js";
import users from "./routers/users.js";
import auth from "./routers/auth.js";
import resume from "./routers/resume.js";
import portfolio from "./routers/portfolio.js";

const router = express.Router();


router.use("/contact_form", contactForm);
router.use("/users", users);
router.use("/auth", auth);
router.use("/resume", resume);
router.use("/portfolio", portfolio);

router.get("*", (req, res) => {
    return res.status(404).json({message: "not found"})
  });

export default router