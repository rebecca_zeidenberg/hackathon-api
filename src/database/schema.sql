drop database if exists projectdatabase;
create database projectdatabase;
use projectdatabase;

create table contactinfo (
	id int not null auto_increment primary key,
    name varchar(255) not null,
    email varchar(255) not null,
    phoneNumber varchar(255) not null,
    message text
);

create table portfolio (
	id int not null auto_increment primary key,
    image text not null,
    description text not null
);

create table resume (
	id int not null auto_increment primary key,
    title varchar(255) not null,
    image text,
    description text
);