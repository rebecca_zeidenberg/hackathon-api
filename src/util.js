import fs from "fs";
import { createRequire } from 'module';
const require = createRequire(import.meta.url);
let jwt = require("jsonwebtoken");

export const checkFile = (filePath, cb) => {
    fs.access(filePath, (err) => {
        if (err) {
            fs.writeFile(filePath, JSON.stringify([]), () => {
                console.log(`${filePath} created`)
                if (cb) {
                    cb()
                }
            })
        }
    });
}

export const createTestUser = () => {
    const user = {
        id: 1,
        name: "test user",
        email: "address@email.com",
        password: "$2b$10$Pf.h0kK.xQApA2Jv/1pNe.0W4us5tVf1Z5eF46nIPtVXgjLP017/."
    }
    fs.writeFile("users.json", JSON.stringify([user]), (err) => {
        if (err) console.log("error creating test user")
        console.log("test user created")
    }) 
}

export const checkToken = (req, res, next) => {
    const header = req.headers['authorization'];

    if(typeof header !== "undefined") {
        const bearer = header.split(" ");
        const token = bearer[1];

        jwt.verify(token, "string", (err) => {
            if(err){
                return res.status(403).json({message: "Token not provided"});
            } else {
                next();
            }
        })

    } else {
        res.status(403).json({message: "Token not provided"})
    }
}