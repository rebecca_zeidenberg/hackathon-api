import express from "express";
import { checkToken } from "../util.js";
import { v4 as uuidv4 } from "uuid";
import { getEntries, getEntryById, createEntry, deleteEntry, updateEntry } from "./mysqlAPI.js";

const router = express.Router();


router.get("/entries/:id", (req, res) => {
    const id = req.params.id;
    getEntryById(id, (data, err) => {
        if (err) {
            return res.status(400).json(err)
        }
        if (!data) {
            return res.status(403).json({message: `entry ${id} not found`})
        }
        return res.status(200).json(data)
    })
})

router.get("/entries", checkToken, (req, res) => {
    getEntries((data, err) => {
        if (err) {
           return res.status(400).json(err)
        }
        return res.status(200).json(data)
    })
})

router.post("/entries", (req, res) => {
    const name = req.body.name
    const email = req.body.email
    const phoneNumber = req.body.phoneNumber
    const content = req.body.content

    const missingFields = []
    if (!name) {
        missingFields.push("name")
    }

    if (!email || !email.includes("@") ){
        missingFields.push("email")
    }

    if (!phoneNumber) {
        missingFields.push("phoneNumber")
    }

    if(!content) {
        missingFields.push("content")
    }

    const errorResponse = {
        message: "validation error",
        invalid: missingFields 
    }    
    
    if (missingFields.length > 0) {
        return res.status(400).json(errorResponse)
    
    }

    let newEntry = {
        id: uuidv4(),
        name: req.body.name,
        email: req.body.email,
        phoneNumber: req.body.phoneNumber,
        content: req.body.content
    };

    createEntry(newEntry, (err) => {
        if (err) {
            return res.status(400).json(err)
        }
        return res.status(201).json(newEntry)
    })
})

router.delete("/entries/:id", (req, res) => {
    const id = req.params.id
    
    deleteEntry(id, (err) => {
        if (err) {
            return res.status(400).json(err)
        }
        return res.status(200).json()
    })
})

router.put("/entries/:id", (req, res) => {
    const id = req.params.id
    const name = req.body.name
    const email = req.body.email
    const phoneNumber = req.body.phoneNumber
    const message = req.body.content

    const entry = {
        id, name, email, phoneNumber, message
    }

    updateEntry(entry, (err) => {
        if (err) {
            return res.status(400).json(err)
        }
        return res.status(200).json(entry)
    })
})

export default router