import express from "express";
import { checkToken } from "../util.js";
import connection from "../database/connection.js";

const router = express.Router();

router.get("/", (req, res) => {
    connection.query("select * from portfolio", (err, rows) => {
        if (err) {
            return res.status(400).json(err)
        }
        return res.status(200).json(rows)
    })
})

router.get("/:id", (req, res) => {
    connection.query("select * from portfolio where id = ?", [req.params.id], (err, rows) => {
        if (err) {
            return res.status(400).json(err)
        }
        return res.status(200).json(rows[0])
    })
})


router.post("/", (req, res) => {
    //validation//
    connection.query("insert into portfolio (image, description) values (?, ?)", [req.body.image, req.body.description], (err) => {
        if (err) {
            return res.status(400).json(err)
        }
        return res.status(201).json(req.body) //get something?????//
    })
})

router.put("/:id", (req, res) => {
    const id = req.params.id
    const image = req.body.image
    const description = req.body.description

    const portfolio = {
        id, image, description
    }

    connection.query("update portfolio set image = ?, description = ? where id = ?", [image, description, id], (err) => {
        if (err) {
            return res.status(400).json(err)
        }
        return res.status(200).json(portfolio)
    })
})

router.delete("/:id", (req, res) => {
    const id = req.params.id

    connection.query("delete from portfolio where id=?", [id], (err) => {
        if (err) {
            return res.status(400).json(err)
        }
        return res.status(200).json()
    })
})


export default router