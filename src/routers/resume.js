import express from "express";
import { checkToken } from "../util.js";
import connection from "../database/connection.js";

const router = express.Router();

router.get("/", (req, res) => {
    connection.query("select * from resume", (err, rows) => {
        if (err) {
            console.log(err)
            return res.status(500).json({message: "server not available"})
        }
        return res.status(200).json(rows)
    })
})

router.get("/:id", (req, res) => {
    connection.query("select * from resume where id = ?", [req.params.id], (err, rows) => {
        if (err) {
            return res.status(400).json(err)
        }
        return res.status(200).json(rows[0])
    })
})

//add token later//
router.post("/", (req, res) => {
    //validation//
    connection.query("insert into resume (title, image, description) values (?, ?, ?)", [req.body.title, req.body.image, req.body.description], (err) => {
        if (err) {
            return res.status(400).json(err)
        }
        return res.status(201).json(req.body) //get something?????//
    })
})

router.put("/:id", (req, res) => {
    const id = req.params.id
    const title = req.body.title
    const image = req.body.image
    const description = req.body.description

    console.log(req.body)
    const resume = {
        id, title, image, description
    }
    console.log(resume)

    connection.query("update resume set title = ?, image = ?, description = ? where id = ?", [title, image, description, id], (err) => {
        if (err) {
            return res.status(400).json(err)
        }
        return res.status(200).json(resume)
    })
})

router.delete("/:id", (req, res) => {
    const id = req.params.id

    connection.query("delete from resume where id=?", [id], (err) => {
        if (err) {
            return res.status(400).json(err)
        }
        return res.status(200).json()
    })
})


export default router