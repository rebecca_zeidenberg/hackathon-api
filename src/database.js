import fs from "fs";
import util from "util";

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

export const getEntries = () => {
    return readFile("entries.json") 
    .then(data => JSON.parse(data))
}

export const getEntryById = id => {
   return getEntries()
    .then(entries => {
        const entry = entries.find(entry => entry.id === id)
        return entry
    }) 
}

export const createEntry = entry => {
    return getEntries()
    .then(entries => {
        entries.push(entry)
        writeFile("entries.json", JSON.stringify(entries))
        .then(data => {
            console.log("entryCreated")
        })
    })
}

const getUsers = () => {
    return readFile("users.json") 
    .then(data => JSON.parse(data))
}

export const createUser = user => {
    return getUsers()
    .then(users => {
        users.push(user)
        writeFile("users.json", JSON.stringify(users))
        .then(data => {
            console.log("userCreated")
        })
    })
}

export const getUserByEmail = email => {
    return getUsers()
    .then(users => {
        const user = users.find(user => user.email === email)
        return user
    })
}