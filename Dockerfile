FROM node:15.2.1

WORKDIR /app

COPY package*.json .

RUN npm install

COPY . .

ENV PORT=8080

EXPOSE 8080

COPY wait-for-it.sh wait-for-it.sh

RUN chmod +x wait-for-it.sh

CMD npm start