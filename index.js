import express from 'express';
import router from './src/router.js';
import {checkFile, createTestUser} from './src/util.js';
import cors from 'cors'
import bodyParser from 'body-parser'
// import {checkFile, createTestUser} from './src/util';


const app = express();
const port = 8080;

const mysql = require('mysql')
const myConnection  = require('express-myconnection')

const config = require('./config')
const dbOptions = {
	host:	  config.database.host,
	user: 	  config.database.user,
	password: config.database.password,
	port: 	  config.database.port, 
	database: config.database.db
}

const db = require('knex')({
    client: 'mysql',
    connection: {
      host : process.env.DATABASE_HOST,
      user : process.env.DATABASE_USER,
      password : process.env.DATABASE_PASSWORD,
      database : process.env.DATABASE_NAME
    }
  })

const main = require('./controllers/main')

app.use(helmet())
app.use(cors())
app.use(express.json())
app.use(router)
app.use(bodyParser.json())
app.use(myConnection(mysql, dbOptions, 'pool'))
app.set('view engine', 'ejs')
app.use(morgan('combined'))

const expressValidator = require('express-validator')
app.use(expressValidator())

const index = require('./index')
const users = require('./src/routers/users')

// checkFile("entries.json")
// checkFile("users.json", createTestUser)

app.listen(process.env.PORT || 8080, () => {
    console.log(`app is running on port ${process.env.PORT || 8080}`)
  })

module.exports = {
    app,
    db
  }