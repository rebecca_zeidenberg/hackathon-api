import connection from "../database/connection.js"
import fs from "fs";
import util from "util";

export const getEntries = (cb) => {
    connection.query("select * from contactinfo", (err, rows) => {
        cb(rows, err)
    })
}

export const getEntryById = (id, cb) => {
    connection.query("select * from contactinfo where id=?", [id], (err, rows) => {
        cb(rows[0], err)
    })
}

export const createEntry = (newEntry, cb) => {
    connection.query(
        "insert into contactinfo (name, email, phoneNumber, message) values (?, ?, ?, ?)", 
        [newEntry.name, newEntry.email, newEntry.phoneNumber, newEntry.content], 
        (err) => {cb(err)}
    )
}

export const deleteEntry = (id, cb) => {
    connection.query("delete from contactinfo where id=?", 
        [id], 
        (err) => {cb(err)}
    )
}

export const updateEntry = (entry, cb) => {
    connection.query("update contactinfo set name = ?, email = ?, phoneNumber = ?, message = ? where id = ?",
    [entry.name, entry.email, entry.phoneNumber, entry.message, entry.id],
        (err) => {cb(err)}
    )
}